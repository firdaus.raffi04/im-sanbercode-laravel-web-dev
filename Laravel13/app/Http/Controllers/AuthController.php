<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function table()
    {
        return view('element.content');
    }

    public function datatable()
    {
        return view('element.content2');
    }

    public function welcome(Request $request)
    {
        $fnama = $request['fnama'];
        $lnama = $request['lnama'];
        $gender = $request['gender'];
        $negara = $request['negara'];
        $bahasa = $request['bahasa'];
        $bio = $request['bio'];

        return view('page.welcome', ['fnama' => $fnama, 'lnama'=> $lnama, 'gender' => $gender, 'negara' => $negara, 'bahasa' => $bahasa, 'bio' => $bio]);
    }
}
