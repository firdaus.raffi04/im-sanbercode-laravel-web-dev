<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign up Form</h2>
    <form action="/submit" method="post">
      @csrf
      <label>First name:</label><br />
      <input type="text" name="fnama" /><br /><br />
      <label>Last name:</label><br />
      <input type="text" name="lnama" /><br /><br />
      <label>Gender:</label><br /><br />
      <input type="radio" id="gender" name="gender" value="Male" /> Male <br />
      <input type="radio" id="gender" name="gender" value="Female" /> Female <br />
      <input type="radio" id="gender" name="gender" value="Other" /> Other <br /><br />
      <label>Nationality:</label><br /><br />
      <select name="negara">
        <option value="Indonesia">Indonesia</option>
        <option value="Singapore">Singapore</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Brunei">Brunei</option></select
      ><br /><br />
      <label>Language Spoken:</label><br /><br />
      <input type="checkbox" name="bahasa" value="Bahasa Indonesia" />Bahasa Indonesia <br /><input type="checkbox" name="bahasa" value="English" />English <br /><input type="checkbox" name="bahasa" />Other <br /><br />
      <label>Bio:</label><br /><br />
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br /><br />
      <input type="submit" value="Sign Up" />
    </form>
</body>
</html>