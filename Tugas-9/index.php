<?php
require "animal.php";
require "frog.php";
require "ape.php";

$sheep = new Animal("shaun");

echo "Nama : " , $sheep->name , "<br>"; // "shaun"
echo "Legs : " , $sheep->legs , "<br>"; // 4
echo "Cold Blooded : " , $sheep->cold_blooded , "<br><br>"; // "no"

$kodok = new Frog("buduk");
echo "Nama : " , $kodok->name , "<br>"; // "shaun"
echo "Legs : " , $kodok->legs , "<br>"; // 4
echo "Cold Blooded : " , $kodok->cold_blooded , "<br>"; // "no"
echo "Jump : " , $kodok->jump(); // "hop hop"
echo "<br><br>"; // "no"

$sungokong = new Ape("kera sakti");
echo "Nama : " , $sungokong->name , "<br>"; // "shaun"
echo "Legs : " , $sungokong->legs , "<br>"; // 2
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
echo "Yell : " ,  $sungokong->yell() ; // "Auooo"